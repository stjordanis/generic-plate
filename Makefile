.PHONY: setup

setup:
	stack setup

.PHONY: build

build:
	stack build

.PHONY: dev

dev:
	stack test --fast --file-watch

.PHONY: test

test:
	stack test

.PHONY: bench

bench:
	stack build && stack exec -- bench --output=results.html

.PHONY: bench2

bench2:
	stack build && stack exec -- bench2 --output=results2.html

.PHONY: clean_all

clean_all:
	stack clean && stack purge

