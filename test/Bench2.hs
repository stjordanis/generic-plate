{-# LANGUAGE FlexibleContexts, TypeApplications, LambdaCase, DeriveGeneric, DeriveDataTypeable, DeriveAnyClass #-}

import Data.Data
import Control.DeepSeq
import GHC.Generics
import Criterion.Main

import qualified Data.Generics.Uniplate.Data as U
import qualified Data.Generics.Plated as G

main :: IO ()
main = defaultMain
  [ bgroup "custom" $
    [ bgroup "universeBi" $
      zipWith
      ( \ n l' -> bgroup ("E L " <> show n)
        [ bench "generic-plate" $ nf (G.universeBi @E' @L') l'
        , bench "uniplate" $ whnf (U.universeBi @E' @L') l'
        ] 
      ) [1..] ls

    , bgroup "universeBi" $
      zipWith
        ( \n expr -> bgroup ("expr " ++ show n)
          [ bench "generic-plate"  $ nf (G.universeBi @Expr' @Int) expr
          , bench "uniplate"  $ nf (U.universeBi @Expr' @Int) expr
          ]
        )
        [1..]
        exprs

    , bgroup "universe" $
      zipWith
        ( \n expr -> bgroup ("expr " ++ show n)
          [ bench "generic-plate"  $ nf G.universe expr
          , bench "uniplate"  $ nf U.universe expr
          ]
        )
        [1..]
        exprs

    , bgroup "transformBi" $
      zipWith
        ( \ n l' -> bgroup ("E L " <> show n)
          [ bench "generic-plate" $ nf (G.transformBi f) l'
          , bench "uniplate" $ nf (U.transformBi f) l'
          ] 
        ) [1..] ls

    , bgroup "transform" $
      zipWith
        ( \n expr -> bgroup ("expr " ++ show n)
          [ bench "generic-plate"  $ nf (G.transformBi opts) expr
          , bench "uniplate"  $ nf (U.transform opts) expr
          ]
        )
        [1..]
        exprs
    ]
  ]



data Expr'
  = Lit' Int
  | Add' Expr' Expr'
  | Mul' Expr' Expr'
  | Div' Expr' Expr'
  | Sub' Expr' Expr'
  | Neg' Expr'
  deriving (Show, Eq, Data, NFData, Generic)

eval :: Expr' -> [Int] -> Int
eval e r = case (e, r) of
  (Lit' i, _) -> i
  (Add'{}, [r1, r2]) -> r1 + r2
  (Mul'{}, [r1, r2]) -> r1 * r2
  (Div'{}, [r1, r2]) -> r1 `mod` r2
  (Sub'{}, [r1, r2]) -> r1 - r2
  (Neg'{}, [r1]) -> 0 - r1

opts :: Expr' -> Expr'
opts = \case
  Add' (Lit' 0) e -> e
  Add' e (Lit' 0) -> e
  Mul' (Lit' 1) e -> e
  Mul' e (Lit' 1) -> e
  Mul' (Lit' 0) _ -> Lit' 0
  Mul' _ (Lit' 0) -> Lit' 0
  Neg' (Neg' e) -> e
  e -> e


data E'
  = L' L'
  | E'2 L' E' E'
  deriving (Show, Eq, Data, NFData, Generic)

data L'
  = I' Int
  | A'
  | B'
  | E' E'
  deriving (Show, Eq, Data, NFData, Generic)

f :: L' -> L'
f = \case
  A' -> B'
  B' -> I' 0
  I' 6 -> A'
  x -> x

ls :: [E']
ls =
  [ E'2 A' (L' (E' (L' A'))) (E'2 (I' 6) (L' B') (L' (E' (L' (E' (L' (I' 1)))))))
  , E'2 (I' 6)
    (E'2 A' (L' (E' (L' A'))) (E'2 B' (L' B') (L' (E' (L' (E' (L' (I' 1))))))))
    (E'2 B' (L' (E' (L' A'))) (E'2 A' (L' B') (L' (E' (L' (I' 1))))))
  , E'2 B'
    ( E'2 (I' 7)
      (E'2 (I' 7) (L' (E' (L' A'))) (E'2 (I' 1) (L' B') (L' (E' (L' (E' (L' (I' 1))))))))
      (E'2 (E' (L' A')) (L' A') (E'2 (I' 1) (L' B') (L' (E' (L' (E' (L' (I' 1))))))))
    )
    ( E'2 B'
      (E'2 A' (L' (E' (L' A'))) (E'2 A' (L' B') (L' (E' (L' (E' (L' (I' 1))))))))
      (E'2 B' (L' (E' (L' A'))) (E'2 A' (L' B') (L' (E' (L' (E' (L' (I' 1))))))))
    )
  ]


exprs :: [Expr']
exprs =
  [ Lit' 1
  , Neg' (Neg' (Lit' 1))
  , Add' (Lit' 1) (Lit' 2)
  , Mul' (Lit' 3) (Add' (Lit' 1) (Lit' 2))
  , Sub'
    (Mul' (Lit' 3) (Add' (Lit' 1) (Lit' 2)))
    (Div' (Lit' 3) (Add' (Mul' (Lit' 3) (Add' (Lit' 1) (Lit' 2))) (Lit' 2)))
  , Add'
    ( Sub'
      (Neg' (Mul' (Lit' 3) (Add' (Lit' 1) (Lit' 2))))
      (Div' (Lit' 3) (Add' (Mul' (Lit' 3) (Add' (Lit' 1) (Lit' 2))) (Lit' 2)))
    )
    (Div' (Mul' (Lit' 3) (Add' (Lit' 1) (Lit' 2))) (Add' (Mul' (Lit' 3) (Add' (Lit' 1) (Lit' 2))) (Lit' 2)))
  , Add'
    ( Sub'
      (Neg' (Mul' (Lit' 0) (Add' (Lit' 1) (Lit' 2))))
      (Div' (Lit' 3) (Add' (Mul' (Lit' 3) (Add' (Lit' 1) (Lit' 2))) (Lit' 2)))
    )
    (Div' (Mul' (Lit' 1) (Add' (Lit' 1) (Lit' 2))) (Add' (Mul' (Lit' 3) (Add' (Lit' 1) (Lit' 2))) (Lit' 2)))
  , Add'
    ( Sub'
      (Neg' (Neg' (Mul' (Lit' 0) (Add' (Lit' 1) (Lit' 2)))))
      (Div' (Lit' 3) (Add' (Mul' (Lit' 3) (Add' (Lit' 1) (Lit' 2))) (Lit' 2)))
    )
    (Div' (Mul' (Lit' 1) (Add' (Lit' 1) (Lit' 2))) (Add' (Mul' (Lit' 3) (Add' (Lit' 1) (Lit' 2))) (Lit' 2)))
  , Mul'
     (Add'
      ( Sub'
        (Neg' (Mul' (Lit' 0) (Add' (Lit' 1) (Lit' 2))))
        (Div' (Lit' 3) (Add' (Mul' (Lit' 3) (Add' (Lit' 1) (Lit' 2))) (Lit' 2)))
      )
      (Div' (Mul' (Lit' 1) (Add' (Lit' 1) (Lit' 2))) (Add' (Mul' (Lit' 3) (Add' (Lit' 1) (Lit' 2))) (Lit' 2)))
    ) (Add'
      ( Sub'
        (Neg' (Neg' (Mul' (Lit' 0) (Add' (Lit' 1) (Lit' 2)))))
        (Div' (Lit' 3) (Add' (Mul' (Lit' 3) (Add' (Lit' 1) (Lit' 2))) (Lit' 2)))
      )
      (Div' (Mul' (Lit' 1) (Add' (Lit' 1) (Lit' 2))) (Add' (Mul' (Lit' 3) (Add' (Lit' 1) (Lit' 2))) (Lit' 2)))
    )
  ]

