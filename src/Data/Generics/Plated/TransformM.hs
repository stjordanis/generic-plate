

{-# LANGUAGE MultiParamTypeClasses, FlexibleInstances, FlexibleContexts, UndecidableInstances #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE DefaultSignatures #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE MonoLocalBinds #-}
{-# LANGUAGE QuantifiedConstraints #-}

module Data.Generics.Plated.TransformM where

import GHC.Generics
import Data.Int
import Data.Word
import qualified Data.Text as T
import qualified Data.Text.Lazy as TL
import qualified Data.ByteString as BS
import qualified Data.ByteString.Lazy as BSL



------------------
-- TransformBiM --
------------------

class Monad m => TransformBiM m on from where
  transformBiM :: (on -> m on) -> from -> m from
  default transformBiM :: (Monad m, Generic from, GTransformM m on (Rep from)) => (on -> m on) -> from -> m from
  transformBiM f x = to <$> gtransformM f (from x)
  {-# inline transformBiM #-}

instance {-# overlapping #-} (Monad m, Generic on, GTransformM m on (Rep on)) => TransformBiM m on on where
  transformBiM f x = f =<< fmap to (gtransformM f (from x))
  {-# inline transformBiM #-}

instance {-# overlapping #-} (Monad m, Generic from, GTransformM m on (Rep from)) => TransformBiM m on from

transformM :: (Monad m, TransformBiM m on on) => (on -> m on) -> on -> m on
transformM = transformBiM

----------------
-- GTransform --
----------------

class Monad m => GTransformM m on struct where
  gtransformM :: (on -> m on) -> struct x -> m (struct x)

instance Monad m => GTransformM m a U1 where
  gtransformM _ U1 = pure U1
  {-# inline gtransformM #-}

instance Monad m => GTransformM m a V1 where
  gtransformM _ x = pure x
  {-# inline gtransformM #-}

instance {-# OVERLAPPING #-} (Monad m, TransformRecM m on on) => GTransformM m on (Rec0 on) where
  gtransformM f (K1 a) = fmap K1 (f =<< transformRecM f a)
  {-# inline gtransformM #-}

instance (Monad m, TransformRecM m on from) => GTransformM m on (K1 _1 from) where
  gtransformM f (K1 from) = K1 <$> transformRecM f from
  {-# inline gtransformM #-}

instance (Monad m, GTransformM m on x, GTransformM m on y) => GTransformM m on (x :+: y) where
  gtransformM f = \case
    L1 x -> L1 <$> gtransformM f x
    R1 y -> R1 <$> gtransformM f y
  {-# inline gtransformM #-}

instance (Monad m, GTransformM m on x, GTransformM m on y) => GTransformM m on (x :*: y) where
  gtransformM f (x :*: y) = (:*:) <$> gtransformM f x <*> gtransformM f y
  {-# inline gtransformM #-}

instance (Monad m, GTransformM m on struct) => GTransformM m on (M1 _x _y struct) where
  gtransformM f (M1 a) = M1 <$> gtransformM f a
  {-# inline gtransformM #-}

-------------------
-- TransformRecM --
-------------------

class Monad m => TransformRecM m on from where
  transformRecM :: (on -> m on) -> from -> m from
  default transformRecM :: (Monad m, Generic from, GTransformM m on (Rep from)) => (on -> m on) -> from -> m from
  transformRecM f x = to <$> gtransformM f (from x)
  {-# inline transformRecM #-}

instance {-# OVERLAPPING #-} (Monad m, Generic from, GTransformM m on (Rep from)) => TransformRecM m on from where
  transformRecM f x = to <$> gtransformM f (from x)
  {-# inline transformRecM #-}

instance {-# OVERLAPPING #-} (Monad m, Generic on, GTransformM m on (Rep on)) => TransformRecM m on on where
  transformRecM f x = to <$> gtransformM f (from x)
  {-# inline transformRecM #-}


-------------------------------
-- Primitive Types Instances --
-------------------------------

instance {-# overlapping #-} Monad m => TransformRecM m on Bool where
  transformRecM _ x = pure x
  {-# inline transformRecM #-}

instance {-# overlapping #-} Monad m => TransformRecM m on Char where
  transformRecM _ x = pure x
  {-# inline transformRecM #-}

instance {-# overlapping #-} Monad m => TransformRecM m on Float where
  transformRecM _ x = pure x
  {-# inline transformRecM #-}

instance {-# overlapping #-} Monad m => TransformRecM m on Double where
  transformRecM _ x = pure x
  {-# inline transformRecM #-}

instance {-# overlapping #-} Monad m => TransformRecM m on Int where
  transformRecM _ x = pure x
  {-# inline transformRecM #-}

instance {-# overlapping #-} Monad m => TransformRecM m on Int8 where
  transformRecM _ x = pure x
  {-# inline transformRecM #-}

instance {-# overlapping #-} Monad m => TransformRecM m on Int16 where
  transformRecM _ x = pure x
  {-# inline transformRecM #-}

instance {-# overlapping #-} Monad m => TransformRecM m on Int32 where
  transformRecM _ x = pure x
  {-# inline transformRecM #-}

instance {-# overlapping #-} Monad m => TransformRecM m on Int64 where
  transformRecM _ x = pure x
  {-# inline transformRecM #-}

instance {-# overlapping #-} Monad m => TransformRecM m on Integer where
  transformRecM _ x = pure x
  {-# inline transformRecM #-}

instance {-# overlapping #-} Monad m => TransformRecM m on Word where
  transformRecM _ x = pure x
  {-# inline transformRecM #-}

instance {-# overlapping #-} Monad m => TransformRecM m on Word8 where
  transformRecM _ x = pure x
  {-# inline transformRecM #-}

instance {-# overlapping #-} Monad m => TransformRecM m on Word16 where
  transformRecM _ x = pure x
  {-# inline transformRecM #-}

instance {-# overlapping #-} Monad m => TransformRecM m on Word32 where
  transformRecM _ x = pure x
  {-# inline transformRecM #-}

instance {-# overlapping #-} Monad m => TransformRecM m on Word64 where
  transformRecM _ x = pure x
  {-# inline transformRecM #-}

instance {-# overlapping #-} Monad m => TransformRecM m on T.Text where
  transformRecM _ x = pure x
  {-# inline transformRecM #-}

instance {-# overlapping #-} Monad m => TransformRecM m on TL.Text where
  transformRecM _ x = pure x
  {-# inline transformRecM #-}

instance {-# overlapping #-} Monad m => TransformRecM m on BS.ByteString where
  transformRecM _ x = pure x
  {-# inline transformRecM #-}

instance {-# overlapping #-} Monad m => TransformRecM m on BSL.ByteString where
  transformRecM _ x = pure x
  {-# inline transformRecM #-}
