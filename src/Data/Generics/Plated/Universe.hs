
{-# LANGUAGE MultiParamTypeClasses, FlexibleInstances, FlexibleContexts, UndecidableInstances #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE DefaultSignatures #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE MonoLocalBinds #-}

module Data.Generics.Plated.Universe where

import GHC.Generics
import Data.DList
import Data.Int
import Data.Word
import qualified Data.Text as T
import qualified Data.Text.Lazy as TL
import qualified Data.ByteString as BS
import qualified Data.ByteString.Lazy as BSL


----------------
-- UniverseBi --
----------------

class UniverseBi from to where
  universeBi :: from -> [to]
  default universeBi :: (Generic from, GUniverse (Rep from) to) => from -> [to]
  universeBi x = toList (guniverse (from x))

instance {-# overlapping #-} (Generic from, GUniverse (Rep from) from) => UniverseBi from from where
  universeBi x = x : toList (guniverse (from x))

instance {-# overlapping #-} (Generic from, GUniverse (Rep from) to) => UniverseBi from to

universe :: UniverseBi from from => from -> [from]
universe = universeBi

---------------
-- GUniverse --
---------------

class GUniverse struct to where
  guniverse :: struct from -> DList to

instance GUniverse U1 to where
  guniverse _ = mempty
  {-# INLINE guniverse #-}

instance GUniverse V1 to where
  guniverse _ = mempty
  {-# INLINE guniverse #-}

instance {-# OVERLAPPING #-} UniverseRec to to => GUniverse (Rec0 to) to where
  guniverse (K1 a) = a `cons` universeRec a
  {-# INLINE guniverse #-}

instance UniverseRec from to => GUniverse (Rec0 from) to where
  guniverse (K1 a) = universeRec a
  {-# INLINE guniverse #-}

instance (GUniverse x to, GUniverse y to) => GUniverse (x :*: y) to where
  guniverse (x :*: y) = guniverse x <> guniverse y
  {-# INLINE guniverse #-}

instance (GUniverse x to, GUniverse y to) => GUniverse (x :+: y) to where
  guniverse = \case
    L1 x -> guniverse x
    R1 y -> guniverse y
  {-# INLINE guniverse #-}

instance GUniverse struct to => GUniverse (M1 _x _y struct) to where
  guniverse (M1 a) = guniverse a

-----------------
-- UniverseRec --
-----------------

class UniverseRec from to where
  universeRec :: from -> DList to
  default universeRec :: (Generic from, GUniverse (Rep from) to) => from -> DList to
  universeRec x = guniverse (from x)
  {-# INLINE universeRec #-}

instance {-# overlapping #-} (Generic from, GUniverse (Rep from) to) => UniverseRec from to

-------------------------------
-- Primitive Types Instances --
-------------------------------

instance {-# overlapping #-} UniverseRec Bool on where
  universeRec _ = mempty
  {-# inline universeRec #-}

instance {-# overlapping #-} UniverseRec Char on where
  universeRec _ = mempty
  {-# inline universeRec #-}

instance {-# overlapping #-} UniverseRec Float on where
  universeRec _ = mempty
  {-# inline universeRec #-}

instance {-# overlapping #-} UniverseRec Double on where
  universeRec _ = mempty
  {-# inline universeRec #-}

instance {-# overlapping #-} UniverseRec Int on where
  universeRec _ = mempty
  {-# inline universeRec #-}

instance {-# overlapping #-} UniverseRec Int8 on where
  universeRec _ = mempty
  {-# inline universeRec #-}

instance {-# overlapping #-} UniverseRec Int16 on where
  universeRec _ = mempty
  {-# inline universeRec #-}

instance {-# overlapping #-} UniverseRec Int32 on where
  universeRec _ = mempty
  {-# inline universeRec #-}

instance {-# overlapping #-} UniverseRec Int64 on where
  universeRec _ = mempty
  {-# inline universeRec #-}

instance {-# overlapping #-} UniverseRec Integer on where
  universeRec _ = mempty
  {-# inline universeRec #-}

instance {-# overlapping #-} UniverseRec Word on where
  universeRec _ = mempty
  {-# inline universeRec #-}

instance {-# overlapping #-} UniverseRec Word8 on where
  universeRec _ = mempty
  {-# inline universeRec #-}

instance {-# overlapping #-} UniverseRec Word16 on where
  universeRec _ = mempty
  {-# inline universeRec #-}

instance {-# overlapping #-} UniverseRec Word32 on where
  universeRec _ = mempty
  {-# inline universeRec #-}

instance {-# overlapping #-} UniverseRec Word64 on where
  universeRec _ = mempty
  {-# inline universeRec #-}

instance {-# overlapping #-} UniverseRec T.Text on where
  universeRec _ = mempty
  {-# inline universeRec #-}

instance {-# overlapping #-} UniverseRec TL.Text on where
  universeRec _ = mempty
  {-# inline universeRec #-}

instance {-# overlapping #-} UniverseRec BS.ByteString on where
  universeRec _ = mempty
  {-# inline universeRec #-}

instance {-# overlapping #-} UniverseRec BSL.ByteString on where
  universeRec _ = mempty
  {-# inline universeRec #-}
