
{-# LANGUAGE MultiParamTypeClasses, FlexibleInstances, FlexibleContexts, UndecidableInstances #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE DefaultSignatures #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE MonoLocalBinds #-}

module Data.Generics.Plated.Children where

import GHC.Generics
import Data.DList
import Data.Int
import Data.Word
import qualified Data.Text as T
import qualified Data.Text.Lazy as TL
import qualified Data.ByteString as BS
import qualified Data.ByteString.Lazy as BSL


----------------
-- ChildrenBi --
----------------

class ChildrenBi from to where
  childrenBi :: from -> [to]
  default childrenBi :: (Generic from, GChildren (Rep from) to) => from -> [to]
  childrenBi x = toList (gchildren (from x))

instance {-# overlapping #-} (Generic from, GChildren (Rep from) to) => ChildrenBi from to

children :: ChildrenBi from from => from -> [from]
children = childrenBi

---------------
-- GChildren --
---------------

class GChildren struct to where
  gchildren :: struct from -> DList to

instance GChildren U1 to where
  gchildren _ = mempty
  {-# INLINE gchildren #-}

instance GChildren V1 to where
  gchildren _ = mempty
  {-# INLINE gchildren #-}

instance {-# OVERLAPPING #-} ChildrenRec to to => GChildren (Rec0 to) to where
  gchildren (K1 a) = singleton a
  {-# INLINE gchildren #-}

instance ChildrenRec from to => GChildren (Rec0 from) to where
  gchildren (K1 a) = childrenRec a
  {-# INLINE gchildren #-}

instance (GChildren x to, GChildren y to) => GChildren (x :*: y) to where
  gchildren (x :*: y) = gchildren x <> gchildren y
  {-# INLINE gchildren #-}

instance (GChildren x to, GChildren y to) => GChildren (x :+: y) to where
  gchildren = \case
    L1 x -> gchildren x
    R1 y -> gchildren y
  {-# INLINE gchildren #-}

instance GChildren struct to => GChildren (M1 _x _y struct) to where
  gchildren (M1 a) = gchildren a

-----------------
-- ChildrenRec --
-----------------

class ChildrenRec from to where
  childrenRec :: from -> DList to
  default childrenRec :: (Generic from, GChildren (Rep from) to) => from -> DList to
  childrenRec x = gchildren (from x)
  {-# INLINE childrenRec #-}

instance {-# overlapping #-} (Generic from, GChildren (Rep from) to) => ChildrenRec from to

-------------------------------
-- Primitive Types Instances --
-------------------------------

instance {-# overlapping #-} ChildrenRec Bool on where
  childrenRec _ = mempty
  {-# inline childrenRec #-}

instance {-# overlapping #-} ChildrenRec Char on where
  childrenRec _ = mempty
  {-# inline childrenRec #-}

instance {-# overlapping #-} ChildrenRec Float on where
  childrenRec _ = mempty
  {-# inline childrenRec #-}

instance {-# overlapping #-} ChildrenRec Double on where
  childrenRec _ = mempty
  {-# inline childrenRec #-}

instance {-# overlapping #-} ChildrenRec Int on where
  childrenRec _ = mempty
  {-# inline childrenRec #-}

instance {-# overlapping #-} ChildrenRec Int8 on where
  childrenRec _ = mempty
  {-# inline childrenRec #-}

instance {-# overlapping #-} ChildrenRec Int16 on where
  childrenRec _ = mempty
  {-# inline childrenRec #-}

instance {-# overlapping #-} ChildrenRec Int32 on where
  childrenRec _ = mempty
  {-# inline childrenRec #-}

instance {-# overlapping #-} ChildrenRec Int64 on where
  childrenRec _ = mempty
  {-# inline childrenRec #-}

instance {-# overlapping #-} ChildrenRec Integer on where
  childrenRec _ = mempty
  {-# inline childrenRec #-}

instance {-# overlapping #-} ChildrenRec Word on where
  childrenRec _ = mempty
  {-# inline childrenRec #-}

instance {-# overlapping #-} ChildrenRec Word8 on where
  childrenRec _ = mempty
  {-# inline childrenRec #-}

instance {-# overlapping #-} ChildrenRec Word16 on where
  childrenRec _ = mempty
  {-# inline childrenRec #-}

instance {-# overlapping #-} ChildrenRec Word32 on where
  childrenRec _ = mempty
  {-# inline childrenRec #-}

instance {-# overlapping #-} ChildrenRec Word64 on where
  childrenRec _ = mempty
  {-# inline childrenRec #-}

instance {-# overlapping #-} ChildrenRec T.Text on where
  childrenRec _ = mempty
  {-# inline childrenRec #-}

instance {-# overlapping #-} ChildrenRec TL.Text on where
  childrenRec _ = mempty
  {-# inline childrenRec #-}

instance {-# overlapping #-} ChildrenRec BS.ByteString on where
  childrenRec _ = mempty
  {-# inline childrenRec #-}

instance {-# overlapping #-} ChildrenRec BSL.ByteString on where
  childrenRec _ = mempty
  {-# inline childrenRec #-}
