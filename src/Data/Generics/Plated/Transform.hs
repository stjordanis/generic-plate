

{-# LANGUAGE MultiParamTypeClasses, FlexibleInstances, FlexibleContexts, UndecidableInstances #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE DefaultSignatures #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE MonoLocalBinds #-}

module Data.Generics.Plated.Transform where

import GHC.Generics
import Data.Int
import Data.Word
import qualified Data.Text as T
import qualified Data.Text.Lazy as TL
import qualified Data.ByteString as BS
import qualified Data.ByteString.Lazy as BSL

  
-----------------
-- TransformBi --
-----------------

class TransformBi on from where
  transformBi :: (on -> on) -> from -> from
  default transformBi :: Generic from => GTransform on (Rep from) => (on -> on) -> from -> from
  transformBi f x = to (gtransform f (from x))
  {-# inline transformBi #-}

instance {-# overlapping #-} (Generic on, GTransform on (Rep on)) => TransformBi on on where
  transformBi f x = f (to (gtransform f (from x)))
  {-# inline transformBi #-}

instance {-# overlapping #-} (Generic from, GTransform on (Rep from)) => TransformBi on from

transform :: TransformBi on on => (on -> on) -> on -> on
transform = transformBi

----------------
-- GTransform --
----------------

class GTransform on struct where
  gtransform :: (on -> on) -> struct x -> struct x

instance GTransform a U1 where
  gtransform _ U1 = U1
  {-# inline gtransform #-}

instance GTransform a V1 where
  gtransform _ x = x
  {-# inline gtransform #-}

instance {-# OVERLAPPING #-} TransformRec on on => GTransform on (Rec0 on) where
  gtransform f (K1 a) = K1 (f $ transformRec f a)
  {-# inline gtransform #-}

instance TransformRec on from => GTransform on (K1 _1 from) where
  gtransform f (K1 from) = K1 (transformRec f from)
  {-# inline gtransform #-}

instance (GTransform on x, GTransform on y) => GTransform on (x :+: y) where
  gtransform f = \case
    L1 x -> L1 $ gtransform f x
    R1 y -> R1 $ gtransform f y
  {-# inline gtransform #-}

instance (GTransform on x, GTransform on y) => GTransform on (x :*: y) where
  gtransform f (x :*: y) = gtransform f x :*: gtransform f y
  {-# inline gtransform #-}

instance GTransform on struct => GTransform on (M1 _x _y struct) where
  gtransform f (M1 a) = M1 $ gtransform f a
  {-# inline gtransform #-}

------------------
-- TransformRec --
------------------

class TransformRec on from where
  transformRec :: (on -> on) -> from -> from
  default transformRec :: Generic from => GTransform on (Rep from) => (on -> on) -> from -> from
  transformRec f x = to (gtransform f (from x))
  {-# inline transformRec #-}

instance {-# OVERLAPPING #-} (Generic from, GTransform on (Rep from)) => TransformRec on from where
  transformRec f x = to (gtransform f (from x))
  {-# inline transformRec #-}

instance {-# OVERLAPPING #-} (Generic on, GTransform on (Rep on)) => TransformRec on on

-------------------------------
-- Primitive Types Instances --
-------------------------------

instance {-# overlapping #-} TransformRec on Bool where
  transformRec _ x = x
  {-# inline transformRec #-}

instance {-# overlapping #-} TransformRec on Char where
  transformRec _ x = x
  {-# inline transformRec #-}

instance {-# overlapping #-} TransformRec on Float where
  transformRec _ x = x
  {-# inline transformRec #-}

instance {-# overlapping #-} TransformRec on Double where
  transformRec _ x = x
  {-# inline transformRec #-}

instance {-# overlapping #-} TransformRec on Int where
  transformRec _ x = x
  {-# inline transformRec #-}

instance {-# overlapping #-} TransformRec on Int8 where
  transformRec _ x = x
  {-# inline transformRec #-}

instance {-# overlapping #-} TransformRec on Int16 where
  transformRec _ x = x
  {-# inline transformRec #-}

instance {-# overlapping #-} TransformRec on Int32 where
  transformRec _ x = x
  {-# inline transformRec #-}

instance {-# overlapping #-} TransformRec on Int64 where
  transformRec _ x = x
  {-# inline transformRec #-}

instance {-# overlapping #-} TransformRec on Integer where
  transformRec _ x = x
  {-# inline transformRec #-}

instance {-# overlapping #-} TransformRec on Word where
  transformRec _ x = x
  {-# inline transformRec #-}

instance {-# overlapping #-} TransformRec on Word8 where
  transformRec _ x = x
  {-# inline transformRec #-}

instance {-# overlapping #-} TransformRec on Word16 where
  transformRec _ x = x
  {-# inline transformRec #-}

instance {-# overlapping #-} TransformRec on Word32 where
  transformRec _ x = x
  {-# inline transformRec #-}

instance {-# overlapping #-} TransformRec on Word64 where
  transformRec _ x = x
  {-# inline transformRec #-}

instance {-# overlapping #-} TransformRec on T.Text where
  transformRec _ x = x
  {-# inline transformRec #-}

instance {-# overlapping #-} TransformRec on TL.Text where
  transformRec _ x = x
  {-# inline transformRec #-}

instance {-# overlapping #-} TransformRec on BS.ByteString where
  transformRec _ x = x
  {-# inline transformRec #-}

instance {-# overlapping #-} TransformRec on BSL.ByteString where
  transformRec _ x = x
  {-# inline transformRec #-}
