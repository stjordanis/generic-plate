

{-# LANGUAGE MultiParamTypeClasses, FlexibleInstances, FlexibleContexts, UndecidableInstances #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE DefaultSignatures #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE MonoLocalBinds #-}
{-# LANGUAGE QuantifiedConstraints #-}

module Data.Generics.Plated.DescendM where

import GHC.Generics
import Data.Int
import Data.Word
import qualified Data.Text as T
import qualified Data.Text.Lazy as TL
import qualified Data.ByteString as BS
import qualified Data.ByteString.Lazy as BSL



------------------
-- DescendBiM --
------------------

class Monad m => DescendBiM m on from where
  descendBiM :: (on -> m on) -> from -> m from
  default descendBiM :: (Monad m, Generic from, GDescendM m on (Rep from)) => (on -> m on) -> from -> m from
  descendBiM f x = to <$> gdescendM f (from x)
  {-# inline descendBiM #-}

instance {-# overlapping #-} (Monad m, Generic from, GDescendM m on (Rep from)) => DescendBiM m on from
  
----------------
-- GDescend --
----------------

class Monad m => GDescendM m on struct where
  gdescendM :: (on -> m on) -> struct x -> m (struct x)

instance Monad m => GDescendM m a U1 where
  gdescendM _ U1 = pure U1
  {-# inline gdescendM #-}

instance Monad m => GDescendM m a V1 where
  gdescendM _ x = pure x
  {-# inline gdescendM #-}

instance {-# OVERLAPPING #-} (Monad m, DescendRecM m on on) => GDescendM m on (Rec0 on) where
  gdescendM f (K1 a) = K1 <$> f a
  {-# inline gdescendM #-}

instance (Monad m, DescendRecM m on from) => GDescendM m on (K1 _1 from) where
  gdescendM f (K1 from) = K1 <$> descendRecM f from
  {-# inline gdescendM #-}

instance (Monad m, GDescendM m on x, GDescendM m on y) => GDescendM m on (x :+: y) where
  gdescendM f = \case
    L1 x -> L1 <$> gdescendM f x
    R1 y -> R1 <$> gdescendM f y
  {-# inline gdescendM #-}

instance (Monad m, GDescendM m on x, GDescendM m on y) => GDescendM m on (x :*: y) where
  gdescendM f (x :*: y) = (:*:) <$> gdescendM f x <*> gdescendM f y
  {-# inline gdescendM #-}

instance (Monad m, GDescendM m on struct) => GDescendM m on (M1 _x _y struct) where
  gdescendM f (M1 a) = M1 <$> gdescendM f a
  {-# inline gdescendM #-}

-------------------
-- DescendRecM --
-------------------

class Monad m => DescendRecM m on from where
  descendRecM :: (on -> m on) -> from -> m from
  default descendRecM :: (Monad m, Generic from, GDescendM m on (Rep from)) => (on -> m on) -> from -> m from
  descendRecM f x = to <$> gdescendM f (from x)
  {-# inline descendRecM #-}

instance {-# OVERLAPPING #-} (Monad m, Generic from, GDescendM m on (Rep from)) => DescendRecM m on from where
  descendRecM f x = to <$> gdescendM f (from x)
  {-# inline descendRecM #-}

instance {-# OVERLAPPING #-} (Monad m, Generic on, GDescendM m on (Rep on)) => DescendRecM m on on where
  descendRecM f x = to <$> gdescendM f (from x)
  {-# inline descendRecM #-}


-------------------------------
-- Primitive Types Instances --
-------------------------------

instance {-# overlapping #-} Monad m => DescendRecM m on Bool where
  descendRecM _ x = pure x
  {-# inline descendRecM #-}

instance {-# overlapping #-} Monad m => DescendRecM m on Char where
  descendRecM _ x = pure x
  {-# inline descendRecM #-}

instance {-# overlapping #-} Monad m => DescendRecM m on Float where
  descendRecM _ x = pure x
  {-# inline descendRecM #-}

instance {-# overlapping #-} Monad m => DescendRecM m on Double where
  descendRecM _ x = pure x
  {-# inline descendRecM #-}

instance {-# overlapping #-} Monad m => DescendRecM m on Int where
  descendRecM _ x = pure x
  {-# inline descendRecM #-}

instance {-# overlapping #-} Monad m => DescendRecM m on Int8 where
  descendRecM _ x = pure x
  {-# inline descendRecM #-}

instance {-# overlapping #-} Monad m => DescendRecM m on Int16 where
  descendRecM _ x = pure x
  {-# inline descendRecM #-}

instance {-# overlapping #-} Monad m => DescendRecM m on Int32 where
  descendRecM _ x = pure x
  {-# inline descendRecM #-}

instance {-# overlapping #-} Monad m => DescendRecM m on Int64 where
  descendRecM _ x = pure x
  {-# inline descendRecM #-}

instance {-# overlapping #-} Monad m => DescendRecM m on Integer where
  descendRecM _ x = pure x
  {-# inline descendRecM #-}

instance {-# overlapping #-} Monad m => DescendRecM m on Word where
  descendRecM _ x = pure x
  {-# inline descendRecM #-}

instance {-# overlapping #-} Monad m => DescendRecM m on Word8 where
  descendRecM _ x = pure x
  {-# inline descendRecM #-}

instance {-# overlapping #-} Monad m => DescendRecM m on Word16 where
  descendRecM _ x = pure x
  {-# inline descendRecM #-}

instance {-# overlapping #-} Monad m => DescendRecM m on Word32 where
  descendRecM _ x = pure x
  {-# inline descendRecM #-}

instance {-# overlapping #-} Monad m => DescendRecM m on Word64 where
  descendRecM _ x = pure x
  {-# inline descendRecM #-}

instance {-# overlapping #-} Monad m => DescendRecM m on T.Text where
  descendRecM _ x = pure x
  {-# inline descendRecM #-}

instance {-# overlapping #-} Monad m => DescendRecM m on TL.Text where
  descendRecM _ x = pure x
  {-# inline descendRecM #-}

instance {-# overlapping #-} Monad m => DescendRecM m on BS.ByteString where
  descendRecM _ x = pure x
  {-# inline descendRecM #-}

instance {-# overlapping #-} Monad m => DescendRecM m on BSL.ByteString where
  descendRecM _ x = pure x
  {-# inline descendRecM #-}
