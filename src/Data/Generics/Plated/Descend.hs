
{-# LANGUAGE MultiParamTypeClasses, FlexibleInstances, FlexibleContexts, UndecidableInstances #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE DefaultSignatures #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE MonoLocalBinds #-}

module Data.Generics.Plated.Descend where

import GHC.Generics
import Data.Int
import Data.Word
import qualified Data.Text as T
import qualified Data.Text.Lazy as TL
import qualified Data.ByteString as BS
import qualified Data.ByteString.Lazy as BSL

  
-----------------
-- DescendBi --
-----------------

class DescendBi on from where
  descendBi :: (on -> on) -> from -> from
  default descendBi :: Generic from => GDescend on (Rep from) => (on -> on) -> from -> from
  descendBi f x = to (gdescend f (from x))
  {-# inline descendBi #-}

instance {-# overlapping #-} (Generic from, GDescend on (Rep from)) => DescendBi on from

descend :: DescendBi on on => (on -> on) -> on -> on
descend = descendBi

----------------
-- GDescend --
----------------

class GDescend on struct where
  gdescend :: (on -> on) -> struct x -> struct x

instance GDescend a U1 where
  gdescend _ U1 = U1
  {-# inline gdescend #-}

instance GDescend a V1 where
  gdescend _ x = x
  {-# inline gdescend #-}

instance {-# OVERLAPPING #-} DescendRec on on => GDescend on (Rec0 on) where
  gdescend f (K1 a) = K1 (f a)
  {-# inline gdescend #-}

instance DescendRec on from => GDescend on (K1 _1 from) where
  gdescend f (K1 from) = K1 (descendRec f from)
  {-# inline gdescend #-}

instance (GDescend on x, GDescend on y) => GDescend on (x :+: y) where
  gdescend f = \case
    L1 x -> L1 $ gdescend f x
    R1 y -> R1 $ gdescend f y
  {-# inline gdescend #-}

instance (GDescend on x, GDescend on y) => GDescend on (x :*: y) where
  gdescend f (x :*: y) = gdescend f x :*: gdescend f y
  {-# inline gdescend #-}

instance GDescend on struct => GDescend on (M1 _x _y struct) where
  gdescend f (M1 a) = M1 $ gdescend f a
  {-# inline gdescend #-}

------------------
-- DescendRec --
------------------

class DescendRec on from where
  descendRec :: (on -> on) -> from -> from
  default descendRec :: Generic from => GDescend on (Rep from) => (on -> on) -> from -> from
  descendRec f x = to (gdescend f (from x))
  {-# inline descendRec #-}

instance {-# OVERLAPPING #-} (Generic from, GDescend on (Rep from)) => DescendRec on from

-------------------------------
-- Primitive Types Instances --
-------------------------------

instance {-# overlapping #-} DescendRec on Bool where
  descendRec _ x = x
  {-# inline descendRec #-}

instance {-# overlapping #-} DescendRec on Char where
  descendRec _ x = x
  {-# inline descendRec #-}

instance {-# overlapping #-} DescendRec on Float where
  descendRec _ x = x
  {-# inline descendRec #-}

instance {-# overlapping #-} DescendRec on Double where
  descendRec _ x = x
  {-# inline descendRec #-}

instance {-# overlapping #-} DescendRec on Int where
  descendRec _ x = x
  {-# inline descendRec #-}

instance {-# overlapping #-} DescendRec on Int8 where
  descendRec _ x = x
  {-# inline descendRec #-}

instance {-# overlapping #-} DescendRec on Int16 where
  descendRec _ x = x
  {-# inline descendRec #-}

instance {-# overlapping #-} DescendRec on Int32 where
  descendRec _ x = x
  {-# inline descendRec #-}

instance {-# overlapping #-} DescendRec on Int64 where
  descendRec _ x = x
  {-# inline descendRec #-}

instance {-# overlapping #-} DescendRec on Integer where
  descendRec _ x = x
  {-# inline descendRec #-}

instance {-# overlapping #-} DescendRec on Word where
  descendRec _ x = x
  {-# inline descendRec #-}

instance {-# overlapping #-} DescendRec on Word8 where
  descendRec _ x = x
  {-# inline descendRec #-}

instance {-# overlapping #-} DescendRec on Word16 where
  descendRec _ x = x
  {-# inline descendRec #-}

instance {-# overlapping #-} DescendRec on Word32 where
  descendRec _ x = x
  {-# inline descendRec #-}

instance {-# overlapping #-} DescendRec on Word64 where
  descendRec _ x = x
  {-# inline descendRec #-}

instance {-# overlapping #-} DescendRec on T.Text where
  descendRec _ x = x
  {-# inline descendRec #-}

instance {-# overlapping #-} DescendRec on TL.Text where
  descendRec _ x = x
  {-# inline descendRec #-}

instance {-# overlapping #-} DescendRec on BS.ByteString where
  descendRec _ x = x
  {-# inline descendRec #-}

instance {-# overlapping #-} DescendRec on BSL.ByteString where
  descendRec _ x = x
  {-# inline descendRec #-}
