
{-# LANGUAGE MultiParamTypeClasses, FlexibleInstances, FlexibleContexts, UndecidableInstances #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE DefaultSignatures #-}
{-# LANGUAGE ConstraintKinds #-}
{-# LANGUAGE MonoLocalBinds #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE LambdaCase #-}

module Data.Generics.PlatedM
  ( module Data.Generics.Plated.TransformM
  , module Data.Generics.Plated.DescendM
  , paraM
  , rewriteM
  , rewriteBiM
  )
where

import Data.Generics.Plated.TransformM
import Data.Generics.Plated.DescendM

import Data.Generics.Plated.Children
import Control.Monad

paraM :: Monad m => ChildrenBi on on => (on -> [result] -> m result) -> on -> m result
paraM f x = f x <=< traverse (paraM f) . children $ x

rewriteM :: (Monad m, TransformBiM m on on) => (on -> m (Maybe on)) -> on -> m on
rewriteM f = transformM g
    where g x = f x >>= maybe (pure x) (rewriteM f)

rewriteBiM :: (Monad m, DescendBiM m on from, TransformBiM m on on) => (on -> m (Maybe on)) -> from -> m from
rewriteBiM f = descendBiM (rewriteM f)
