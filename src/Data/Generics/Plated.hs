
{-# LANGUAGE MultiParamTypeClasses, FlexibleInstances, FlexibleContexts, UndecidableInstances #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE DefaultSignatures #-}
{-# LANGUAGE ConstraintKinds #-}
{-# LANGUAGE MonoLocalBinds #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE LambdaCase #-}

module Data.Generics.Plated
  ( module Data.Generics.Plated.Transform
  , module Data.Generics.Plated.Descend
  , module Data.Generics.Plated.Children
  , module Data.Generics.Plated.Universe
  , para
  , rewrite
  , rewriteBi
  )
where

import Data.Generics.Plated.Transform
import Data.Generics.Plated.Descend
import Data.Generics.Plated.Children
import Data.Generics.Plated.Universe

para :: ChildrenBi on on => (on -> [result] -> result) -> on -> result
para f x = f x . map (para f) . children $ x

rewrite :: TransformBi on on => (on -> Maybe on) -> on -> on
rewrite f = transform g
    where g x = maybe x (rewrite f) (f x)

rewriteBi :: (DescendBi on from, TransformBi on on) => (on -> Maybe on) -> from -> from
rewriteBi f = descendBi (rewrite f)

